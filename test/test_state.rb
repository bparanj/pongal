require 'test/unit'
require_relative '../lib/pongal/faker'

class TestFaker < Test::Unit::TestCase
  
  def test_generate_random_us_state
    state = Pongal::Faker.state
    
   assert_send([Pongal::Faker::STATES, :member?, state])
  end
  
end
