# coding: utf-8
lib = File.expand_path("../lib/", __FILE__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)
require "pongal/version"

Gem::Specification.new do |spec|
  spec.author = ["Bala Paranj"]
  spec.description = %q(Pongal is a valid US state generator that can be used in Geocoding applications.)
  spec.email = "support@zepho.com"
  spec.executables = %w[pongal]
  spec.files = %w[CHANGELOG.md LICENSE.md README.md pongal.gemspec]
  spec.files += Dir.glob("bin/**/*")
  spec.files += Dir.glob("lib/**/*.rb")
  spec.files += Dir.glob("test/**/*")
  spec.homepage = "http://rubyplus.com/"
  spec.licenses = %w[MIT]
  spec.name = "pongal"
  spec.require_paths = %w[lib]
  spec.required_rubygems_version = ">= 1.3.5"
  spec.summary = 'Pongal is a simple gem for generating a valid random US state.'
  spec.test_files = Dir["test/test*.rb"]
  spec.version = Pongal::VERSION
end
