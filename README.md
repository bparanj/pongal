Pongal
====

Pongal is a simple gem for generating a valid random US state. 

Description
-----------

This is useful in Geocoding applications that need to geocode. Use this gem to seed data in your applications. 

Installation
------------
    gem install pongal

Usage and documentation
-----------------------

To get a random state, call the state method like this:
	Pongal::Faker.state
	
To run the tests:
     ruby test/test_state.rb 	

[homepage]: http://www.rubyplus.com/

License
-------
Released under the MIT License.  See the [LICENSE][] file for further details.

[license]: LICENSE.md
